import os
import sys
import feedparser
from datetime import datetime 
import json
import requests
import csv

from operator import itemgetter

def main():

  ##########################    Get Lists   #########################

  RSS_list = getBreachesFromRSS()
  API_list = getBreachesFromAPI() 
  CSV_list = getBreachesFromCSV()
  
  ##########################    Uniqify Lists    ####################
 
  # TODO: lists need contain only unique items
  # after sorting to make it faster  

  ##########################    Unify lists   #######################

  final_list = []
  for i in range(0, len(RSS_list)):
    final_list.append(RSS_list[i])
  for i in range(0, len(API_list)):
    final_list.append(API_list[i])
  for i in range(0, len(CSV_list)):
    final_list.append(CSV_list[i])

  sorted_breaches_list = [list(x) for x in set(tuple(x) for x in final_list)]
  sorted_breaches_list = sorted(sorted_breaches_list, key=itemgetter(1))
  
  #########################    Create JSON objects from list ########

  JSON_OBJECT = ''
  for i in range(0,len(sorted_breaches_list)):
    JSON_OBJECT = '%s\n{"name":"%s", "records": "%d", "year": "%d"}' % (JSON_OBJECT, str(sorted_breaches_list[i][0]),sorted_breaches_list[i][1],sorted_breaches_list[i][2])   
 
  # print JSON_OBJECT to file

  print len(sorted_breaches_list)

  f = open('data_breaches.txt','w')
  f.write(JSON_OBJECT)
  f.close()


# RSS gives only 2018 breaches currently
def getBreachesFromRSS():

  python_wiki_rss_url = "http://feeds.feedburner.com/HaveIBeenPwnedLatestBreaches?format=xml"

  d = feedparser.parse( python_wiki_rss_url )
  title = d.feed.title

  entrynumber = len(d.entries)
  
  list_all = []
  for x in range(0, entrynumber):
    entry = d.entries[x]
    fulldate = entry.published.split(",")[1][1:-1]

    objDate = datetime.strptime(fulldate, '%d %b %Y %H:%M:%S ')
    records = int(entry.title.split("-")[1].split("breached accounts")[0][1:-1].replace(',',''))

    name = entry.title.split("-")[0][:-1]    

    #if ('unverified' in name):
    #  name = str(entry.title.split("-")[0][:-1].split('(unverified)')[0][:-2])
    #else:
    #  name = str(entry.title.split("-")[0][:-1])
   
    #RSS_list_records.append(records)
    #RSS_list_name.append(name)
    
    list_all.append([name, records, objDate.year])
  
  return list_all


def getBreachesFromAPI():

  r = requests.get('https://haveibeenpwned.com/api/v2/breaches')
  jsondata = json.loads(r.text)

  list_all = []
  for x in range(0, len(jsondata)):
    breachDate = jsondata[x]['BreachDate']
    year = datetime.strptime(breachDate, '%Y-%m-%d').year
    records = int(str(jsondata[x]['PwnCount']).replace(',','')) 
    name = jsondata[x]['Title'].encode('ascii','ignore')    

    #if (records in RSS_list_records and records != 0 and name in RSS_list_name):
    #  continue

    list_all.append([name, records, year])

  return list_all

def getBreachesFromCSV():

  url = 'https://www.privacyrights.org/data-breaches/Privacy_Rights_Clearinghouse-Data-Breaches-Export.csv?title=&taxonomy_vocabulary_11_tid%5B0%5D=2436&taxonomy_vocabulary_11_tid%5B1%5D=2434&taxonomy_vocabulary_11_tid%5B2%5D=2257'

  data = requests.get(url)
  list_all = []
  
  with open('out.csv', 'w') as f:
    writer = csv.writer(f)
    reader = csv.reader(data.text.encode('utf-8').splitlines())

    for row in reader:
        writer.writerow(row)
        
        year = ''       
        record = 0
        try: 
	  year = datetime.strptime(row[0], '%B %d, %Y').year
 	  record = int(row[5].replace(',',''))
        except (ValueError) as e:
	  continue
                
        list_all.append([row[1], record, year])
    return list_all


if __name__ == '__main__':
  main()
